# bPanel 4 Payment

Módulo para añadir formas de pago a bPanel4.

## Registrar forma de pago

Para registrar una forma de pago, desde el comando de instalación del plugin que añade
la forma de pago habrá que llamar a:

```php
// $paymentMethod será un método de pago, que debe implementar la interfaz
// \Bittacora\Bpanel4\Payment\Contracts\PaymentMethod

$paymentModule = new Bittacora\Bpanel4\Payment\Payment();
$paymentModule->registerPaymentMethod($paymentMethod);
```
