<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Tests\Models;

use Bittacora\Bpanel4\Payment\Contracts\OrderDetailsDto;
use Bittacora\Bpanel4\Payment\Contracts\PaymentMethod;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;

/**
 * @method static self create()
 */
final class TestPaymentMethod extends Model implements PaymentMethod
{
    /**
     * @var string
     */
    public $table = 'test_payment_methods';

    public function getName(): string
    {
        return 'Método de pago de prueba';
    }

    public function processPayment(OrderDetailsDto $orderDetails): RedirectResponse
    {
        return new RedirectResponse('/');
    }

    public function getUserInstructions(): ?string
    {
        return 'Instrucciones del método de pago';
    }
}
