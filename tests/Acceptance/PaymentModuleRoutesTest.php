<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Tests\Acceptance;

use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class PaymentModuleRoutesTest extends TestCase
{
    use RefreshDatabase;

    public function testMuestraElListadoDeFormasDePago(): void
    {
        $this->actingAs((new UserFactory())->withPermissions('bpanel4-payment.index')->createOne());
        $response = $this->get(route('bpanel4-payment.index'));
        $response->assertOk();
        $response->assertSee('Formas de pago');
    }
}
