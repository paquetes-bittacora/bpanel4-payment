<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Tests\Factories;

use Bittacora\Bpanel4\Payment\Contracts\PaymentMethod;
use Exception;
use Mockery;
use Mockery\MockInterface;

final class PaymentMethodMockFactory
{
    private readonly MockInterface&PaymentMethod $mock;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->mock = Mockery::mock('PaymentMethod' . random_int(1, 1000), PaymentMethod::class);
        $this->mock->shouldReceive('getName')->andReturn('Metodo de pago de prueba')->byDefault();
        $this->mock->shouldReceive('getUserInstructions')->andReturn('Instrucciones de la forma de pago')
            ->byDefault();
        $this->mock->shouldIgnoreMissing();
    }

    public static function default(): MockInterface|PaymentMethod
    {
        return (new self())->getMock();
    }

    public function getMock(): MockInterface&PaymentMethod
    {
        return $this->mock;
    }

    public function withName(string $name): self
    {
        $this->mock->shouldReceive('getName')->andReturn($name);
        return $this;
    }

    public static function new(): self
    {
        return new self();
    }
}
