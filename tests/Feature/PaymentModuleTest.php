<?php

/** @noinspection DynamicInvocationViaScopeResolutionInspection */

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Tests\Feature;

use Bittacora\Bpanel4\Payment\Contracts\PaymentMethod;
use Bittacora\Bpanel4\Payment\Exceptions\PaymentMethodAlreadyRegisteredException;
use Bittacora\Bpanel4\Payment\Models\PaymentMethodRolePermission;
use Bittacora\Bpanel4\Payment\Models\PaymentMethodRow;
use Bittacora\Bpanel4\Payment\Payment;
use Bittacora\Bpanel4\Payment\Tests\Models\TestPaymentMethod;
use Bittacora\Bpanel4\Payment\Tests\Models\TestPaymentMethod2;
use Bittacora\Bpanel4\Payment\Tests\Models\TestPaymentMethod3;
use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Bittacora\Bpanel4Users\Models\User;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

final class PaymentModuleTest extends TestCase
{
    use MockeryPHPUnitIntegration;
    use RefreshDatabase;

    private Payment $paymentModule;

    protected function setUp(): void
    {
        parent::setUp();
        $this->paymentModule = $this->app->make(Payment::class);
    }

    /**
     * @throws PaymentMethodAlreadyRegisteredException
     */
    public function testListaLosMetodosDePagoRegistrados(): void
    {
        $this->registerExamplePaymentMethods();
        $methods = $this->paymentModule->getPaymentMethods();

        self::assertNotEmpty($methods);
        /** @phpstan-ignore-next-line Por tipos siempre será true, pero dejo este assert por seguridad */
        self::assertInstanceOf(PaymentMethod::class, $methods[0]);
    }

    public function testNoSePuedeRegistrar2VecesLaMismaFormaDePago(): void
    {
        self::expectException(PaymentMethodAlreadyRegisteredException::class);
        $paymentMethods = $this->registerExamplePaymentMethods();
        $this->paymentModule->registerPaymentMethod($paymentMethods[0]);
    }

    public function testSoloDevuelveLasFormasDePagoDisponiblesParaElRolDelUsuario(): void
    {
        $paymentMethods = [TestPaymentMethod::create(), TestPaymentMethod2::create(), TestPaymentMethod3::create()];
        /** @var Role $role */
        $role = Role::create(['name' => 'registered']);
        /** @var Authenticatable&User $user */
        $user = (new UserFactory())->withRoles('registered')->createOne();
        $this->actingAs($user);
        $paymentMethodRow = PaymentMethodRow::create(['payment_method' => $paymentMethods[0]::class]);
        PaymentMethodRow::create(['payment_method' => $paymentMethods[0]::class]);
        PaymentMethodRow::create(['payment_method' => $paymentMethods[0]::class]);
        PaymentMethodRolePermission::create([
            'payment_method_id' => $paymentMethodRow->getId(),
            'role_id' => $role->id,
        ]);

        $result = $this->paymentModule->getPaymentMethodsForUser($user);
        self::assertCount(1, $result);
    }

    /**
     * @return array<Mockery\MockInterface&PaymentMethod>
     * @throws PaymentMethodAlreadyRegisteredException
     */
    private function registerExamplePaymentMethods(): array
    {
        $paymentMethod1 = Mockery::mock('PaymentMethod1', PaymentMethod::class);
        $this->paymentModule->registerPaymentMethod($paymentMethod1);
        $paymentMethod2 = Mockery::mock('PaymentMethod2', PaymentMethod::class);
        $this->paymentModule->registerPaymentMethod($paymentMethod2);

        return [
            $paymentMethod1,
            $paymentMethod2,
        ];
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }
}
