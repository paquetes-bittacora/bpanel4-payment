<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Tests\Feature;

use Bittacora\Bpanel4\Payment\Http\Livewire\PaymentMethodRoleCheckbox;
use Bittacora\Bpanel4\Payment\Models\PaymentMethodRolePermission;
use Bittacora\Bpanel4\Payment\Models\PaymentMethodRow;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

final class PaymentMethodRoleCheckboxTest extends TestCase
{
    use RefreshDatabase;

    public function testActivaUnaFormaDePagoParaUnRol(): void
    {
        $role = Role::create(['name' => 'test']);
        $paymentMethodRow = PaymentMethodRow::create(['payment_method' => 'test']);

        // Creamos el componente con un rol y un método de pago, pero sin crear el
        // PaymentMethodRolePermission, es decir, no asociamos el permiso y la forma de pago
        $component = Livewire::test(PaymentMethodRoleCheckbox::class, [
            'roleId' => $role->id,
            'paymentMethodId' => $paymentMethodRow->id,
        ]);
        $component->call('togglePermission');
        self::assertEquals(true, $component->get('status'));
    }

    public function testDesactivaUnaFormaDePagoParaUnRol(): void
    {
        list($role, $paymentMethodRow) = $this->createTestPermission();
        $component = Livewire::test(PaymentMethodRoleCheckbox::class, [
            'roleId' => $role->id,
            'paymentMethodId' => $paymentMethodRow->id,
        ]);
        $component->call('togglePermission');
        self::assertEquals(false, $component->get('status'));
    }

    /**
     * @return array
     */
    private function createTestPermission(): array
    {
        $role = Role::create(['name' => 'test']);
        $paymentMethodRow = PaymentMethodRow::create(['payment_method' => 'test']);
        PaymentMethodRolePermission::create(['role_id' => $role->id, 'payment_method_id' => $paymentMethodRow->id]);
        return array($role, $paymentMethodRow);
    }
}
