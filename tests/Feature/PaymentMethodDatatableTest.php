<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Tests\Feature;

use Bittacora\Bpanel4\Payment\Contracts\OrderDetailsDto;
use Bittacora\Bpanel4\Payment\Contracts\PaymentMethod;
use Bittacora\Bpanel4\Payment\Exceptions\PaymentMethodAlreadyRegisteredException;
use Bittacora\Bpanel4\Payment\Http\Livewire\PaymentMethodsDatatable;
use Bittacora\Bpanel4\Payment\Payment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\RedirectResponse;
use Livewire\Livewire;
use Mockery;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

final class PaymentMethodDatatableTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Role::create(['name' => 'registered']);
    }

    /**
     * @throws PaymentMethodAlreadyRegisteredException
     */
    public function testListaLasFormasDePago(): void
    {
        $this->registerPaymentMethods();
        $datatable = Livewire::test(PaymentMethodsDatatable::class);
        $datatable->assertSee('Metodo de pago 1');
    }

    /**
     * @throws PaymentMethodAlreadyRegisteredException
     */
    private function registerPaymentMethods(): void
    {
        /** @var Payment $paymentModule */
        $paymentModule = $this->app->make(Payment::class);
        $class = $this->getPaymentMethod();
        $paymentModule->registerPaymentMethod($class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    private function getPaymentMethod(): PaymentMethod
    {
        return new class () implements PaymentMethod {
            public function getName(): string
            {
                return 'Metodo de pago 1';
            }

            public function processPayment(OrderDetailsDto $orderDetails): RedirectResponse
            {
            }

            public function getUserInstructions(): ?string
            {
                return '';
            }
        };
    }
}
