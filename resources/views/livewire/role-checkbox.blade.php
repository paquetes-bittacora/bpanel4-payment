<div>
    <div class="form-check form-check-inline text-center">
        <input class="form-check-input ace-switch" type="checkbox" @if($status) checked @endif
            wire:click="togglePermission">
    </div>
</div>
