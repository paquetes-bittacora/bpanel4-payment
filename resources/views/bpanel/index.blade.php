@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-payment::general.index'))

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-payment::general.index') }}</span>
            </h4>
        </div>
        <div class="p-3">

            @livewire('bpanel4-payment::payment-methods-datatable')
            <hr>
            <small>
                <div class="d-flex align-items-center ">
                    <div class="pr-2"><i class="fas fa-info-circle"></i></div>
                    <div><p class="mb-0"> Para ajustar la prioridad de las formas de pago, arrastre y suelte
                             el icono <i class="fas fa-bars"></i> de la fila que quiera mover. Para activar o desactivar una forma de pago para un tipo de cliente, haga clic en los interruptores (<i
                                    class="fas fa-toggle-on"></i>). La forma de pago que esté situada más arriba y que esté activada para un rol, será la forma de pago
                             predeterminada para ese rol.</p></div>
                </div>
            </small>
        </div>
    </div>
@endsection

@push('scripts')
    @vite('vendor/bittacora/bpanel4-panel/resources/assets/js/livewire-sortable.js')
@endpush
