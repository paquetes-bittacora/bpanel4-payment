<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment;

use Bittacora\Bpanel4\Payment\Commands\Bpanel4PaymentInstallCommand;
use Bittacora\Bpanel4\Payment\Http\Livewire\PaymentMethodRoleCheckbox;
use Bittacora\Bpanel4\Payment\Http\Livewire\PaymentMethodsDatatable;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;

final class PaymentServiceProvider extends ServiceProvider
{
    public const PACKAGE_PREFIX = 'bpanel4-payment';

    public function boot(): void
    {
        $this->commands([
            Bpanel4PaymentInstallCommand::class,
        ]);

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);

        $this->registerLivewireComponents();
    }

    public function registerLivewireComponents(): void
    {
        Livewire::component(self::PACKAGE_PREFIX . '::payment-methods-datatable', PaymentMethodsDatatable::class);
        Livewire::component(self::PACKAGE_PREFIX . '::payment-methods-role-checkbox', PaymentMethodRoleCheckbox::class);
    }
}
