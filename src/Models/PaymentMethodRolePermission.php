<?php

namespace Bittacora\Bpanel4\Payment\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Clase que representa si un método de pago está habilitado para un
 * rol de usuario.
 * @property int $payment_method_id
 * @property int $role_id
 * @method static self create(array $properties)
 * @method static Builder whereIn(string $field, Collection $values)
 */
final class PaymentMethodRolePermission extends Model
{
    /**
     * @var string
     */
    public $table = 'payment_methods_roles';

    /**
     * @var string[]
     */
    public $fillable = ['role_id', 'payment_method_id'];
}
