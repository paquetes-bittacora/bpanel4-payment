<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Models;

use Bittacora\Bpanel4\Payment\Contracts\PaymentMethod;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

/**
 * Clase que solo se usa para ordenar las formas de pago en la BD.
 * @property class-string<PaymentMethod> $payment_method
 * @property int $order_column
 * @property int $id
 * @method static self orderBy($column)
 * @method self get()
 * @method array<self> all()
 * @method static self create(array $properties)
 * @method static Builder whereIn(string $field, Collection|array $values)
 * @method static self firstOrCreate(array $search, ?array $additionalFields = null)
 */
final class PaymentMethodRow extends Model implements Sortable
{
    use SortableTrait;

    /** @var string */
    protected $table = 'payment_methods';

    /**
     * @var string[]
     */
    protected $fillable = ['payment_method'];

    public function getPaymentMethodObject(): PaymentMethod
    {
        return resolve($this->payment_method);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPaymentMethod(): string
    {
        return $this->payment_method;
    }

    public function getOrderColumn(): int
    {
        return $this->order_column;
    }
}
