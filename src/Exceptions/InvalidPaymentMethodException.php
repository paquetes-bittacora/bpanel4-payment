<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Exceptions;

use Exception;

final class InvalidPaymentMethodException extends Exception
{
    /**
     * @var string
     */
    protected $message = 'El método de pago no es válido';
}
