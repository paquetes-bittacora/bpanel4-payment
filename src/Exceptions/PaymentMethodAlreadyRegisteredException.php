<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Exceptions;

use Exception;

final class PaymentMethodAlreadyRegisteredException extends Exception
{
    /** @var string $message */
    public $message = 'No se puede volver a registrar una forma de pago';
}
