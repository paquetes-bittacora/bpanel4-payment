<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Validation;

use Bittacora\Bpanel4\Payment\Contracts\PaymentMethod;
use Bittacora\Bpanel4\Payment\Exceptions\PaymentMethodAlreadyRegisteredException;
use Bittacora\Bpanel4\Payment\Payment;

final class PaymentMethodValidator
{
    /**
     * @throws PaymentMethodAlreadyRegisteredException
     */
    public function checkIfMethodIsAlreadyRegistered(PaymentMethod $paymentMethod, Payment $payment): void
    {
        foreach ($payment->getPaymentMethods() as $registeredPaymentMethod) {
            $this->compareShippingMethods($registeredPaymentMethod, $paymentMethod);
        }
    }

    /**
     * @throws PaymentMethodAlreadyRegisteredException
     */
    private function compareShippingMethods(PaymentMethod $paymentMethod1, PaymentMethod $paymentMethod2): void
    {
        if ($paymentMethod1::class === $paymentMethod2::class) {
            throw new PaymentMethodAlreadyRegisteredException();
        }
    }
}
