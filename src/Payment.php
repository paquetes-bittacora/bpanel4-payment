<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment;

use Bittacora\Bpanel4\Payment\Contracts\PaymentMethod;
use Bittacora\Bpanel4\Payment\Exceptions\PaymentMethodAlreadyRegisteredException;
use Bittacora\Bpanel4\Payment\Models\PaymentMethodRolePermission;
use Bittacora\Bpanel4\Payment\Models\PaymentMethodRow;
use Bittacora\Bpanel4\Payment\Validation\PaymentMethodValidator;
use Bittacora\Bpanel4Users\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Spatie\Permission\Models\Role;

final class Payment
{
    public function __construct(
        private readonly PaymentMethodValidator $shippingMethodValidator,
    ) {
    }

    /**
     * Devuelve los métodos de pago disponibles para el usuario.
     * @return array<PaymentMethod>
     */
    public function getPaymentMethodsForUser(?User $user): array
    {
        $paymentMethodRows = PaymentMethodRow::whereIn(
            'id',
            $this->getAvailablePaymentMethodsForUser($user)->pluck('payment_method_id')
        )->orderBy('order_column')->get()->all();

        $output = [];
        foreach ($paymentMethodRows as $paymentMethodRow) {
            $output[] = $paymentMethodRow->getPaymentMethodObject();
        }

        return $output;
    }

    /**
     * Devuelve todos los métodos de pago registrados, sin filtrar por rol de usuario
     * @return array<PaymentMethod>
     */
    public function getPaymentMethods(): array
    {
        $output = [];
        $paymentMethodRows = PaymentMethodRow::orderBy('order_column')->get()->all();

        foreach ($paymentMethodRows as $paymentMethodRow) {
            $output[] = $paymentMethodRow->getPaymentMethodObject();
        }

        return $output;
    }

    /**
     * @throws PaymentMethodAlreadyRegisteredException
     */
    public function registerPaymentMethod(PaymentMethod $paymentMethod): void
    {
        $this->shippingMethodValidator->checkIfMethodIsAlreadyRegistered($paymentMethod, $this);
        PaymentMethodRow::firstOrCreate(['payment_method' => $paymentMethod::class]);
    }

    /**
     * @return Builder<PaymentMethodRolePermission>
     */
    private function getAvailablePaymentMethodsForUser(?User $user): Builder
    {
        if (null === $user) {
            return $this->getPaymentMethodsForAnonymousUser();
        }

        if (!in_array('admin', $user->getRoleNames()->toArray(), true)) {
            return PaymentMethodRolePermission::whereIn('role_id', $user->roles()->pluck('id'));
        }
        return PaymentMethodRolePermission::query();
    }

    private function getPaymentMethodsForAnonymousUser(): Builder
    {
        return PaymentMethodRolePermission::whereIn(
            'role_id',
            [Role::whereName('registered-user')->first()->id]
        );
    }
}
