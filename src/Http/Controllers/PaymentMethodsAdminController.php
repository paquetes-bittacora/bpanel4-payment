<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

final class PaymentMethodsAdminController extends Controller
{
    public function __construct(private readonly Factory $view)
    {
    }

    public function index(): View
    {
        $this->authorize('bpanel4-payment.index');
        return $this->view->make('bpanel4-payment::bpanel.index');
    }
}
