<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Http\Livewire;

use Bittacora\Bpanel4\Payment\Models\PaymentMethodRow;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Spatie\Permission\Models\Role;

final class PaymentMethodsDatatable extends DataTableComponent
{
    public bool $reordering = true;

    public string $reorderingMethod = 'reorder';

    public array $roleIds = [];

    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        $columns = [
            Column::make('Nombre', 'id')->view('bpanel4-payment::livewire.datatable-columns.name'),
        ];

        foreach (Role::all() as $role) {
            $columns[] = Column::make($role->name, 'id')->format(
                    fn($value, $row, Column $column) => view('bpanel4-payment::livewire.datatable-columns.role-checkbox')
                        ->with(['clientRole' => $role, 'row' => $row])
            );
        }

        return $columns;
    }

    public function query(): Builder
    {
        return PaymentMethodRow::query()->select('id', 'payment_method')->orderBy('order_column', 'ASC');
    }

    public function rowView(): string
    {
        return 'bpanel4-payment::livewire.payment-methods';
    }

    public function boot(): void
    {
        $this->roleIds = Role::all()->pluck('id')->toArray();
    }

    public function reorder($list): void
    {
        foreach ($list as $item) {
            PaymentMethodRow::where('id', $item['value'])->update(['order_column' => $item['order']]);
        }
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
