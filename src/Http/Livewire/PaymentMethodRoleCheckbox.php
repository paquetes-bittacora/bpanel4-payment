<?php

namespace Bittacora\Bpanel4\Payment\Http\Livewire;

use Bittacora\Bpanel4\Payment\Models\PaymentMethodRolePermission;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class PaymentMethodRoleCheckbox extends Component
{
    private Factory $view;

    public bool $status = false;

    public $roleId;

    public $paymentMethodId;

    public function booted(Factory $view): void
    {
        $this->status = $this->permissionIsSet();
        $this->view = $view;
    }

    public function render(): View
    {
        return $this->view->make('bpanel4-payment::livewire.role-checkbox');
    }

    public function togglePermission(): void
    {
        $this->permissionIsSet() ? $this->disableForRole() :
            $this->enableForRole();
    }

    private function disableForRole(): void
    {
        $this->getPermission()->delete();
        $this->status = false;
    }

    /**
     * @return void
     */
    private function enableForRole(): void
    {
        PaymentMethodRolePermission::create([
            'role_id' => $this->roleId,
            'payment_method_id' => $this->paymentMethodId,
        ]);
        $this->status = true;
    }

    /**
     * @return mixed
     */
    private function permissionIsSet(): bool
    {
        return null !== $this->getPermission();
    }

    /**
     * @return mixed
     */
    private function getPermission()
    {
        return PaymentMethodRolePermission::where('role_id', $this->roleId)
            ->where('payment_method_id', $this->paymentMethodId)->first();
    }
}
