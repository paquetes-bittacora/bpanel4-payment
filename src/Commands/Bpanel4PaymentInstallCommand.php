<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class Bpanel4PaymentInstallCommand extends Command
{
    private const PERMISSIONS = ['index', 'reorder'];
    /**
     * @var string
     */
    protected $signature = 'bpanel4-payment:install';

    /**
     * @var string
     */
    protected $description = 'Instala el paquete de formas de pago';

    public function handle(AdminMenu $adminMenu): void
    {
        $this->createMenuEntries($adminMenu);
        $this->registerAdminPermissions();
    }

    private function createMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createGroup('ecommerce', 'Tienda', 'far fa-shopping-cart');
        $adminMenu->createModule(
            'ecommerce',
            'bpanel4-payment',
            'Formas de pago',
            'far fa-credit-card-front'
        );

        $adminMenu->createAction(
            'bpanel4-payment',
            'Prioridad de las formas de pago',
            'index',
            'far fa-sort-numeric-down',
            'bpanel4-payment.index'
        );
    }

    private function registerAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        /** @var Role $adminRole */
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'bpanel4-payment.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }
}
