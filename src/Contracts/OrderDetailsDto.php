<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Contracts;

use Bittacora\Bpanel4\Prices\Types\Price;

/**
 * Dto para pasar información de forma genérica a los plugins de formas de pago.
 */
final class OrderDetailsDto
{
    public function __construct(
        public readonly int $orderId,
        public readonly Price $orderAmount,
        public readonly string $additionalDetails,
    ) {
    }
}
