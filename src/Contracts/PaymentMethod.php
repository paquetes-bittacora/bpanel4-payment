<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Contracts;

use Illuminate\Http\RedirectResponse;

/**
 * Interfaz que deben implementar los plugins de formas de pago para ser compatibles con este módulo.
 */
interface PaymentMethod
{
    /**
     * Nombre del método de pago de cara al usuario
     */
    public function getName(): string;

    /**
     * Procesa el pago. Por ejemplo, en pago con tarjeta, llevará a la pasarela de pago (el retorno a las URL OK y KO
     * ya no se contempla en el módulo genérico, sino en el propio plugin de pago con tarjeta)
     *
     * No especifico tipo de retorno porque aunque algunos métodos no devolverán nada, el pago por tarjeta (por ejemplo)
     * necesitará mostrar un formulario, o redirigir a la pasarela, etc.
     */
    public function processPayment(OrderDetailsDto $orderDetails): RedirectResponse;

    /**
     * Muestra instrucciones al usuario en el carrito (por ejemplo, número de cuenta al que hacer una transferencia,
     * etc)
     */
    public function getUserInstructions(): ?string;
}
