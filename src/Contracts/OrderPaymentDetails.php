<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Payment\Contracts;

use DateTime;

/**
 * Interfaz para que los plugins de métodos de pago puedan devolver información sobre un pago.
 */
interface OrderPaymentDetails
{
    /**
     * Indica si el pago se realizó correctamente o no
     */
    public function wasSuccessful(): bool;

    /**
     * Devuelve la fecha en la que se realizó el intento de pago
     */
    public function getDate(): DateTime;

    /**
     * Detalles del pago (por ejemplo, mensaje devuelto por el TPV)
     */
    public function getDetails(): ?string;

    /**
     * Identificador del pago (por ejemplo, el código de operación del TPV)
     */
    public function getPaymentId(): ?string;
}
