<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Payment\Http\Controllers\PaymentMethodsAdminController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel/formas-de-pago')->name('bpanel4-payment.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(static function () {
        Route::get('/listar', [PaymentMethodsAdminController::class, 'index'])->name('index');
    });
