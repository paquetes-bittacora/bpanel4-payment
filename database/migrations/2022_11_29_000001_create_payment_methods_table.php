<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'payment_methods';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->id();
            $table->string('payment_method');
            $table->integer('order_column');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::drop(self::TABLE_NAME);
    }
};
